/**
 * @depend
 *   underscore.js
 *   jQuery
 */
var Utils = function () {

  function _getTranslateY ( selector ) {
    var matrix = $(selector).css('-webkit-transform')
      , _m = /\((.*)\)/.exec(matrix);

    if ( _m == null ) {
      return 0;
    }
    else {
      return +(_((_m[1]||'').split(', ')).last());
    }
  }

  function _getContentsHeight () {
    return Math.max.apply( null, [
        document.body.clientHeight,
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.documentElement.clientHeight
    ]);
  }

  return {
    getTranslateY     : _getTranslateY,
    getContentsHeight : _getContentsHeight
  };

}();
