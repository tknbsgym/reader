reader.directive('onRepeatRender', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      if ( scope.$last === true ) {
        $timeout(function() {
          scope.$emit('render:' + attr['onRepeatRender']);
        });
      }
    }
  };
});
