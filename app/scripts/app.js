'use strict';

angular.module('reader', [])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/reader', {
        templateUrl: 'views/reader/list.html',
        controller: 'ReaderListCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
