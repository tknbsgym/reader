'use strict';

var reader = angular.module('reader')
  .controller('MainCtrl', function ($scope) {
    $scope.owesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });


// service that controlls position of the CLOCK.
reader.factory('Clock', function ($rootScope) {

  var clock = { $el: null
              , $time: null
              , $shorthand: null
              , $longhand: null
              , bodyHeight: 0
              , clockHeight: 0
              , margin: 10
              , contentHeight: 0
              , clockMoveMax: 0
              , watch: false
              , heights: []
              , currentIndex: 0
              };

  clock.initialize = function() {
    this.$el = $('#clock');
    this.$time = $('#time');
    this.$shorthand = $('#shorthand');
    this.$longhand = $('#longhand');
    this.bodyHeight = window.innerHeight;
    this.clockHeight = $('#clock').height();
    this.contentHeight = this.getContentsHeight();
    this.clockMoveMax = this.bodyHeight - this.margin * 2 - this.clockHeight;
    this.scrollMax = this.contentHeight - this.bodyHeight;
    this.calcItemHeights();
    return this;
  };

  // 各アイテムの高さを格納します
  clock.calcItemHeights = function () {
    var _this = this;
    $('#reader-container').children().each(function(idx, item) {
      _this.heights.push( $(item).height() );
    });
  };

  // スクロールを監視します
  clock.observe = function (start) {
    if ( start == null ) {
      start = true;
    }
    start = !!start;

    if ( this.watch === start ) { return; }

    this.watch = start;
    if ( start ) {
      $(window).on('touchmove scroll', this.onScroll);
    }
    else {
      $(window).off('touchmove scroll', this.onScroll);
    }
  };

  // 指定の位置までアニメーションで移動します
  clock.moveTo = function ( y ) {
    this.$el
      .addClass('anim')
      .css('-webkit-transform', 'translate3d(0, ' + y + 'px, 0)')
      .on('webkitTransitionEnd', function () {
        $(this).removeClass('anim');
      });
  };


  // ざっくりスクロールされたときに実行されます
  clock.onScroll = function (event) {
    var scroll = clock.scrollTop() / clock.scrollMax;
    var dy = 0;
    if ( scroll !== 0 ) {
      var dy = clock.clockMoveMax * scroll;
    }
    $('#clock').css('-webkit-transform', 'translateY(' + dy + 'px)');

    clock.updateIndex();
  };

  // スクロールの量を返します
  clock.scrollTop = function () {
    var ty = Utils.getTranslateY('#reader-container');
    return -1 * ty;
  };

  // コンテンツ全体の高さを返します
  clock.getContentsHeight = function () {
    return Math.max.apply( null, [
        document.body.clientHeight,
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.documentElement.clientHeight
    ]);
  };

  // 時計の中心の居場所のインデックスを返します
  clock.getPosition = function (dy) {
    var current;

    if ( dy != null ) {
      current = dy;
    } else {
      current = this.getClockCenter();
    }

    var scroll = this.scrollTop();
    current += scroll;

    var s = 0 , index = 0;
    _.some(this.heights, function (_item) {
      s += _item;
      if ( current <= s ) {
        return false;
      }
      index++;
    });
    return index;
  };

  // 現在の中心のポジションを返します
  clock.getClockCenter = function () {
    var matrix = this.$el.css('-webkit-transform')
      , _m = /\((.*)\)/.exec(matrix);

    if ( _m == null ) { return 0; }

    // 現在のTOP
    var current = +(_((_m[1]||'').split(', ')).last());

    // 中心を返します
    return current + this.margin + this.clockHeight / 2;
  };

  // 時間の表示を更新します
  clock.updateIndex = function (scope) {
    var index = this.getPosition();
    if ( this.currentIndex !== index ) {
      $rootScope.$emit('clock:updateindex', index);
      this.currentIndex = index;
    }
  };

  // 指定された時間まで時計の針を回します。
  clock.time = function (time) {
    if ( ! /^\d{1,2}:\d{1,2}$/.test(time) ) { return; }

    var _t = time.split(':')
      , h = +(_t[0])
      , m = +(_t[1]);

    this.hour(h, m);
    this.minute(m);
  }

  // 短針をまわします
  clock.hour = function (hour, minute) {
    var degHoursMinute = 0.5  /*  360 / 12 / 60  */
      , degHour = 30          /*  360 / 12       */

      , hour = hour >= 12 ? hour - 12 : hour
      , deg = degHour * hour + degHoursMinute * minute;

    rotate('short', deg);
  };

  // 長針をまわします
  clock.minute = function (minute) {
    var degMinute = 6  /*  360 / 60  */
      , deg = degMinute * minute

    rotate('long', deg);
  };

  // スクロールの最大値を返します
  clock.scrollmax = function () {
    return this.scrollMax;
  };

  // 最大まで移動します
  clock.moveToBottom = function () {
    var pos = 0;
    this.moveTo(this.clockMoveMax);
  };

  // 針の回転を実行します
  function rotate (shortlong, deg) {
    clock['$' + shortlong + 'hand'].css('-webkit-transform', 'rotateZ(' + deg + 'deg)');
  }

  return clock;
});
