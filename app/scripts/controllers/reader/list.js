function ReaderListCtrl ($scope, $http, $rootScope, Clock) {

  var CONTAINER_SELECTOR = '#reader-container'
    , $readercontainer = $(CONTAINER_SELECTOR);

  var url = 'http://www.hivelocity.co.jp/feed';
  var rssUrl = 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&callback=JSON_CALLBACK&num=10&q=' + encodeURIComponent(url);


  // タッチをスタートした位置を保持します
  var _startY = 0;

  // スクロールの位置を保持します
  var _scrollPos = 0;

  // スクロールを終了する直前の位置を保持します
  var _scrollEndLastPos = 0;

  // スクロールに慣性をつけるためのタイマーです
  var _scrollTimer;

  // 慣性的なスクロールの終了位置を保持します
  var _scrollTargetPos = 0;


  $http.jsonp(rssUrl)
    .success(function(data) {
      if ( data.responseData == null ) {
        alert('error');
        return;
      }

      $scope.entries = data.responseData.feed.entries;
    });

  $scope.$on('render:items', function () {

    $readercontainer
      .on('touchstart' , touchstart)
      .on('touchmove'  , touchmove )
      .on('touchend'   , touchend  );

    Clock.initialize();
    setTimeout(function(){ updateClock(null, 0) }, 700);
  });

  $rootScope.$on('clock:updateindex', updateClock);


  //
  // private function(s).
  //

  function touchstart (event) {
    if ( _scrollTimer ) {
      clearInterval(_scrollTimer);
      _scrollPos = Utils.getTranslateY( CONTAINER_SELECTOR );
    }

    var touch = event.originalEvent.touches[0];
    _startY = (-1 * _scrollPos) + touch.pageY;
  }

  function touchmove (event) {
    event.preventDefault();
    _scrollEndLastPos = Utils.getTranslateY( CONTAINER_SELECTOR );
    var touch = event.originalEvent.touches[0]
      , dy = -1 * ( _startY - touch.pageY );
    $readercontainer.css('-webkit-transform', 'translate3d(0, ' + dy + 'px, 0)');
    Clock.onScroll();
  }

  function touchend () {
    var pos = Utils.getTranslateY( CONTAINER_SELECTOR );

    // 上に行きすぎたとき
    if ( pos > 0 ) {
      _scrollPos = 0;
      moveTo(0);
      Clock.moveTo(0);
    }

    // 下に行きすぎたとき
    else if ( (-1 * pos) > Clock.scrollmax() ) {
      _scrollPos = -1 * Clock.scrollmax();
      moveTo(_scrollPos);
      Clock.moveToBottom();
    }

    else {
      // 直前の位置との距離
      var dy = _scrollEndLastPos - pos;

      // 慣性の終了位置
      _scrollTargetPos = pos - dy * 20;

      // 慣性を起動します
      _scrollTimer = setInterval( scrollThrow, 10 );
    }

  }

  // エントリのインデックスが変わったときに呼ばれます
  function updateClock (event, index) {
    var entry = $scope.entries[index];

    if ( entry == null ) { return; }

    // 仮
    var time = formatDate(entry.publishedDate);
    $('#time').text(time);

    // 時計をまわす
    Clock.time(time);

  }

  function scrollThrow () {
    var currentPos = Utils.getTranslateY( CONTAINER_SELECTOR );

    // 上に行きすぎたとき
    if ( currentPos > 0 ) {
      _scrollTargetPos = 0;
    }
    // 下に行きすぎたとき
    else if ( -(1 * currentPos) > Clock.scrollmax() ) {
      _scrollTargetPos = -1 * Clock.scrollmax();
    }

    // 慣性的なスクロール
    if ( Math.abs(currentPos - _scrollTargetPos) < 1 ) {
      clearInterval(_scrollTimer);
      _scrollPos = _scrollTargetPos;
    }
    else {
      var moveto = currentPos - (currentPos - _scrollTargetPos) / 20;
      moveToNoAnim(moveto);
    }

    Clock.onScroll();
  }

  // 指定の位置までアニメーションして移動します
  function moveTo ( y ) {
    $readercontainer
      .addClass('anim')
      .css('-webkit-transform', 'translate3d(0, ' + y + 'px, 0)')
      .on('webkitTransitionEnd', function () {
        $(this).removeClass('anim');
      });
  }

  // 指定の位置までアニメーションしないで移動します
  function moveToNoAnim ( y ) {
    $readercontainer.css('-webkit-transform', 'translate3d(0, ' + y + 'px, 0)');
  }

  function formatDate (date) {
    //var _date = new Date(date);
    var time = /\d{2}:\d{2}/.exec(date)[0];
    return time;
  }

}
